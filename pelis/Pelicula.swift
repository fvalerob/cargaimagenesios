//
//  Pelis.swift
//  pelis
//
//  Created by Antonio Pertusa on 14/12/16.
//  Copyright © 2016 Antonio Pertusa. All rights reserved.
//

import UIKit

class Pelicula
{
    var title : String?
    var year : Int?
    var director : String?
    var urlPoster : String?
    var rented : Bool?
    var synopsis : String?
    var image : UIImage?
    
    init(title: String, year: Int, director: String, urlPoster: String, rented: Bool, synopsis: String)
    {
        self.title = title
        self.year = year
        self.director  = director
        self.urlPoster = urlPoster
        self.rented = rented
        self.synopsis = synopsis
    }
}


